package net.jevring.calendarrectifier;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AbstractPromptReceiver;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.java6.auth.oauth2.VerificationCodeReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventReminder;
import com.google.api.services.calendar.model.Events;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Set;

/**
 * Ensures that all events have correct reminders.
 *
 * @author markus@jevring.net
 */
public class CalendarRectifier {
	private final Set<String> excludeSummaries = Set.of("Lunch (do not schedule)");
	private final EventReminder fiveMinutePopup = new EventReminder().setMethod("popup").setMinutes(5);

	public void rectifyCalendar() throws IOException, GeneralSecurityException {
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
		HttpRequestInitializer httpRequestInitializer = requestOrLoadCredentials();
		Calendar calendar = new Calendar.Builder(httpTransport, jsonFactory, httpRequestInitializer).setApplicationName("Calendar rectifier").build();

		// why are all the google libraries like this? why are they so hard to use?! =(
		long now = System.currentTimeMillis();
		DateTime earliestTime = new DateTime(now);
		long oneWeekInMillis = 7 * 24 * 3600 * 1000;
		DateTime latestTime = new DateTime(now + oneWeekInMillis);
		Events events = calendar.events()
		                        .list("primary")
		                        .setMaxResults(500)
		                        .setTimeMin(earliestTime)
		                        .setTimeMax(latestTime)
		                        .setSingleEvents(true)
		                        .setOrderBy("startTime")
		                        .execute();
		for (Event event : events.getItems()) {
			// skip things we know we want to exclude
			if (excludeSummaries.contains(event.getSummary().trim())) {
				continue;
			}

			// skip things that already have an acceptable reminder set
			Event.Reminders reminders = event.getReminders();
			if (hasAcceptableReminder(reminders)) {
				continue;
			}

			// ensure that all-day events have no reminder
			if (event.getStart().getDateTime() == null) {
				if (hasAcceptableReminder(reminders)) {
					reminders.setUseDefault(false);
					reminders.setOverrides(null);
					System.out.printf("Removing reminder for: %s %s%n", event.getStart().getDate(), event.getSummary());
					calendar.events().update("primary", event.getId(), event).execute();
				} else {
					continue;
				}
			}
			System.out.printf("Setting default reminder for: %29s %-20s %s%n", event.getStart().getDateTime(), reminders, event.getSummary());
			reminders.setUseDefault(true);
			reminders.setOverrides(null);
			calendar.events().update("primary", event.getId(), event).execute();
		}
	}

	private boolean hasAcceptableReminder(Event.Reminders reminders) {
		return reminders != null &&
		       reminders.getUseDefault() != null &&
		       (reminders.getUseDefault() || (reminders.getOverrides() != null && reminders.getOverrides().contains(fiveMinutePopup)));
	}

	// wtf does Credential implement HttpRequestInitializer? that makes no fucking sense!
	private Credential requestOrLoadCredentials() throws IOException, GeneralSecurityException {
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
		try (InputStream is = getClass().getResourceAsStream("/credentials.json")) {
			GoogleClientSecrets googleClientSecrets = GoogleClientSecrets.load(jsonFactory, new InputStreamReader(is, StandardCharsets.UTF_8));
			FileDataStoreFactory fileDataStoreFactory = new FileDataStoreFactory(new File("tokens"));
			List<String> scopes = List.of(CalendarScopes.CALENDAR);
			GoogleAuthorizationCodeFlow googleAuthorizationCodeFlow =
					new GoogleAuthorizationCodeFlow.Builder(httpTransport, jsonFactory, googleClientSecrets, scopes).setDataStoreFactory(fileDataStoreFactory)
					                                                                                                .setAccessType("offline")
					                                                                                                .build();

			// presumably this runs in the background and acts as the redirect receiver. 
			// This will not really work for a remote machine. 
			// let's see how this actually works
			//LocalServerReceiver localServerReceiver = new LocalServerReceiver.Builder().setPort(8888).build();

			// this lets me just specify the code via stdin, or a file, for that matter, after the setup is complete
			VerificationCodeReceiver stdin = new AbstractPromptReceiver() {
				@Override
				public String getRedirectUri() throws IOException {
					// this is a magic uri that presents the result in a copy-and-pastable form, similar to 'gcloud init'
					return "urn:ietf:wg:oauth:2.0:oob";
				}
			};
			AuthorizationCodeInstalledApp authorizationCodeInstalledApp = new AuthorizationCodeInstalledApp(googleAuthorizationCodeFlow, stdin);

			// todo: it doesn't seem to matter what string we specify here.
			return authorizationCodeInstalledApp.authorize("whoever");
		}
	}
}
