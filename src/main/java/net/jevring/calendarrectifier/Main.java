package net.jevring.calendarrectifier;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Ensures that all events have correct reminders.
 *
 * @author markus@jevring.net
 */
public class Main {
	public static void main(String[] args) {
		try {
			CalendarRectifier calendarRectifier = new CalendarRectifier();
			calendarRectifier.rectifyCalendar();
		} catch (GeneralSecurityException | IOException e) {
			System.err.println("Could not access calendar because: " + e.getMessage());
			System.exit(-1);
		}
	}
}
