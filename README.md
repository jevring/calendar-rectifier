# Calendar rectifier
This app can be run as a cron job, and will check your calendar for the coming week to ensure that everything has a correct reminder set.
This assumes that the default reminder is acceptable.
This assumes that whole-day events should have no reminder.

You can whitelist certain events. Right now that's done in the `excludeSummaries` set in the `CalendarRectifier` class, but this could be moved to a file, if I wasn't so lazy... =)

The first time you run it should *not* be via cron, as it'll ask you to authenticate against google. It'll show you a URL you should visit in your browser. After visiting that, you'll be presented with a token. Copy that token from the browser and paste it into the waiting prompt in the application. Once this is done, you shouldn't have to do it again, presumably for a long time. I don't know when your token expires, but I think it's a long time in the future. Potentially forever.

Once that's done, you can crontab it, and it'll just run. 